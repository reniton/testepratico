# Testes práticos

## Introdução

Projeto para resolução de testes práticos de automação

## Instalação
Para realizar a instalação do projeto, deve-se ter instalado as seguintes tecnologias:

>Maven
>> **_https://maven.apache.org/download.cgi_**

>MySql
>> **_https://dev.mysql.com/downloads/installer/_**
> <br /> Usuario: root
<br /> Senha: admin
<br /> Ou se preferir, alterar o usuario e senha no projeto (Classe: ConnectionFactory)


> GIT

> Java
>> **_Versão 8++_**

## Teste
Para realizar a execução do projeto deve-se executar os seguintes passos:

1. Criar um banco de dados local no MySql com a seguinte estrutura:

    1.1 Criar Database:
    
        CREATE DATABASE `banco_teste_automacao`;

    1.2 Criar tabela:
        **_CREATE TABLE `massas` (
        `IDMASSAS` int(11) NOT NULL AUTO_INCREMENT,
        `NAME_PRODUCT` varchar(45) DEFAULT NULL,
        `CUSTOMIZATION` varchar(45) DEFAULT NULL,
        `DISPLAY` varchar(600) DEFAULT NULL,
        `DISPLAY_RESOLUTION` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
        `DISPLAY_SIZE` varchar(45) DEFAULT NULL,
        `MEMORY` varchar(45) DEFAULT NULL,
        `OPERATING_SYSTEM` varchar(45) DEFAULT NULL,
        `PROCESSOR` varchar(255) DEFAULT NULL,
        `TOUCHSCREEN` varchar(45) DEFAULT NULL,
        `WEIGHT` varchar(45) DEFAULT NULL,
        `COLOR` varchar(45) DEFAULT NULL,
        PRIMARY KEY (`IDMASSAS`)
        ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;_**

    1.3 Inserir valores na tabela

       insert into
        massas(NAME_PRODUCT,CUSTOMIZATION,DISPLAY,DISPLAY_RESOLUTION,DISPLAY_SIZE,MEMORY,OP
        ERATING_SYSTEM,PROCESSOR,TOUCHSCREEN,WEIGHT,COLOR)
        values("HP PAVILION 15Z TOUCH LAPTOP","Simplicity","15.6-inch diagonal Full HD WLED-backlit Display
        (1920x1080) Touchscreen","1920x1080","15.6","16GB DDR3 - 2 DIMM","Windows 10","AMD Quad-Core A10-
        8700P Processor + AMD Radeon(TM) R6 Graphics","Yes","5.51 lb","GRAY");
        


2. Abrir o cmd e executar o seguinte comando:

``git clone -b https://gitlab.com/reniton/testepratico.git
``


Dentro da pasta raiz do projeto no CMD execute o seguinte código:

1. Para testar a automação:

``mvn clean test -Dtest=Runner
`` 

No projeto contem a classe RecargaTelefoneTeste para os testes unitários.
