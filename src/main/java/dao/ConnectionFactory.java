package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

    private static Connection conn;

    private static String host = "localhost";
    private static String porta = "3306";
    private static String schema = "banco_teste_automacao";
    private static String user = "root";
    private static String senha = "admin";

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        if (conn == null) {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://" + host + ":" + porta + "/" + schema + "?user=" + user + "&password=" + senha);
        }
        return conn;
    }

    public static void closeConnection() throws SQLException {
        if (conn != null) {
            conn.close();
        }
        conn = null;
    }
}
