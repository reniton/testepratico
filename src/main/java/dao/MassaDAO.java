package dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class MassaDAO {

    public void alterarInformacao(String color) {
        try {
            PreparedStatement stmt = ConnectionFactory.getConnection().prepareStatement(
                    "UPDATE massas SET COLOR ='" + color + "' where IDMASSAS = 2");
            stmt.executeUpdate();
            stmt.close();
        } catch (SQLException e) {
            System.err.println("Ocorreu um erro no update da massa no banco");
            System.err.println(e.getMessage());

        } catch (ClassNotFoundException e) {
            System.err.println("Ocorreu um erro");

        }
    }

    public String obterInformacao(String coluna) {
        try {
            PreparedStatement stmt = ConnectionFactory.getConnection().prepareStatement(
                    "SELECT " + coluna + " FROM massas");
            ResultSet rs = stmt.executeQuery();
            if (!rs.next()) return null;
            return rs.getString(1);
        } catch (SQLException e) {
            System.err.println("Ocorreu um erro no select da massa no banco");
            System.err.println(e.getMessage());
            return null;
        } catch (ClassNotFoundException e) {
            System.err.println("Ocorreu um erro");
            return null;
        }

    }


}
