package br.com.testepratico.core;

import br.com.testepratico.config.Configuracao;
import br.com.testepratico.config.ConfigurationManager;
import br.com.testepratico.page.MenuPage;
import br.com.testepratico.page.ProdutoPage;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import dao.MassaDAO;
import io.cucumber.java.AfterAll;
import io.cucumber.java.Scenario;
import org.junit.After;
import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.rules.TestWatcher;
import org.junit.runners.Parameterized;

public class BaseTest extends TestWatcher {
    protected static ProdutoPage produto = new ProdutoPage();
    protected static MenuPage menu = new MenuPage();
    protected static MassaDAO massa = new MassaDAO();
    protected static Configuracao dados = ConfigurationManager.getConfiguration();

    public BaseTest() {
        super();
    }

    @Before
    public void start(String browser) {

        if (browser.equals("Chrome")) {
            Configuration.browser = dados.browser();
        } else if (browser.equals("Firefox")) {
            Configuration.browser = dados.browser();
        }
        //Configuration.driverManagerEnabled = false;
        Configuration.startMaximized = true;
        Configuration.holdBrowserOpen = false;
        Configuration.headless = false;
        Configuration.baseUrl = dados.url();
        Configuration.timeout = dados.timeout();
    }

    public void finish() {
        WebDriverRunner.driver().close();
    }

}
