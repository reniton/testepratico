package br.com.testepratico.core;

import com.codeborne.selenide.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

import com.codeborne.selenide.SelenideElement;

import java.io.File;
import java.util.HashMap;
import java.util.Map;


public class BasePage {

    /* -----------------------------> Javascript <---------------------------------  */
    public String loginComJavascript(String url, String login, String password) {
        Credentials credentials = new Credentials(login, password);
        Selenide.open(url, AuthenticationType.BASIC, credentials);
        return null;
    }

    public BasePage javacriptConfirmar() {
        switchTo().alert().accept();
        return this;
    }

    public BasePage javacriptCancelar() {
        switchTo().alert().dismiss();
        return this;
    }

    public BasePage javacriptEscrever(String valor) {
        switchTo().alert().sendKeys(valor);
        return this;
    }

    public String javacriptObtertexto() {
        return switchTo().alert().getText();
    }

    /* ----------------------------> Clicar <--------------------------------  */
    public BasePage clicarLink(String elemento) {
        $(byLinkText(elemento)).click();
        return this;
    }

    public BasePage clicarBotao(String tagname) {
        $(By.tagName(tagname)).click();
        return this;
    }

    public BasePage clicarElemento(String elemento) {
        $(elemento).click();
        return this;
    }

    public BasePage clicarElementoXpath(String elemento) {
        $(byXpath(elemento)).click();
        return this;
    }

    public String recuperarTextoPorElemento(String elemento) {
        String texto = $(elemento).getText();
        return texto;
    }

    public String recuperarTextoPorXpath(String xpath) {
        String texto = $(By.xpath(xpath)).getText();
        return texto;
    }

    /*-------------------------------> hover <----------------------------*/
    public BasePage hover(String elemento) {
        $(elemento).hover();
        return this;
    }

    public BasePage hoverXpath(String elemento) {
        $(byXpath(elemento)).hover();
        return this;
    }

    /*-------------------------------> Selecionar <----------------------------*/

    /**
     * Seleciona um dropdown, uma lista de valores
     *
     * @param elemento O elemento principal que contem todos os itens da lista
     * @param valor    O valor que voce quer selecionar
     */
    public String selecionarLista(String elemento, String valor) {
        $(elemento).selectOptionContainingText(valor);
        return null;
    }

    public String selecionarlink(String elemento) {
        $(byLinkText(elemento)).click();
        return null;
    }

    public String selecionarRadio(String name, String valor) {
        $(By.name(name)).selectRadio(valor);
        return null;
    }

    public String selecionarCheckbox(String element) {
        $(element).setSelected(true);
        return null;
    }

    public String selecionarCheckboxXpath(String element) {
        $(By.xpath(element)).setSelected(true);
        return null;
    }

    public String deselecionarCheckbox(By element) {
        $(element).setSelected(false);
        return null;
    }

    /**
     * Realiza a importação de um arquivo, que esteja na pasta arquivos em resources
     *
     * @param elemento    o elemento do botão ou link para buscar o arquivo
     * @param nomeArquivo O nome do arquivo que deseja incluir
     */
    public BasePage upload(String elemento, String nomeArquivo) {
        File file = $(elemento).uploadFile(new File("src/main/resources/arquivos/" + nomeArquivo));
        return this;
    }

    public BasePage entrarFrame(String elemento) {
        switchTo().frame(elemento);
        return this;
    }

    public BasePage abrirAba(String url) {
        executeJavaScript("window.open('" + url + "','Nova Aba');");
        switchTo().window("Nova Aba");
        open(url);
        return this;
    }

    public BasePage fecharjanela() {
        String janelaPrincipal = getWebDriver().getWindowHandle();
        Selenide.closeWindow();
        for (String janela : getWebDriver().getWindowHandles()) {
            if (!(janela.equals(janelaPrincipal))) {
                switchTo().window(janela);
            }
        }
        return this;
    }

    /*  ------------------>   métodos de condições  <----------------------     */


    /**
     * Verifica se um item está desabilitado
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemDesativado(String elemento) {
        $(byXpath(elemento)).shouldBe(disabled);
        return this;
    }

    /**
     * Verifica se um elemento HTML é visível na página web
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemVisivel(String elemento) {
        $(byXpath(elemento)).shouldBe(visible);
        return this;
    }

    /**
     * Verifica se um elemento HTML existe na página web (Esse item pode está visivel ou não)
     * no lugar de exist pode-se colocar: present
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemExiste(String elemento) {
        $(elemento).shouldBe(exist);
        return this;
    }

    /**
     * Verifica se um elemento HTML existe na página web (Esse item pode está visivel ou não)
     * no lugar de exist pode-se colocar: present
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public Boolean verificarItemExistePorXpath(String elemento) {
        $(byXpath(elemento)).shouldBe(exist);
        return true;
    }

    /**
     * Verifica se um elemento HTML não existe na página web
     * no lugar de dissappear pode-se colocar: hidden ou not
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemInvisivel(String elemento) {
        $(byXpath(elemento)).shouldBe(disappear);
        return this;
    }

    /**
     * Verifica se um elemento HTML permite somente leitura
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemSomenteLeitura(String elemento) {
        $(elemento).shouldBe(readonly);
        return this;
    }

    /**
     * Verifica se um atributo está presente em um elemento HTML
     *
     * @param atrribute o atributo que deseja verificar que existe
     * @param elemento  o elemento que deseja ser verificado
     */
    public BasePage verificarAttribute(String elemento, String atrribute) {
        $(elemento).shouldNotBe(attribute(atrribute));
        return this;
    }

    /**
     * Verifica se o atributo name possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem a tag name
     * @param valor    o valor que deseja verificar que a tag name tenha
     */
    public BasePage verificarName(String elemento, String valor) {
        $(elemento).shouldBe(name(valor));
        return this;
    }

    /**
     * Verifica se o atributo value possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem a tag value
     * @param valor    o valor que deseja verificar que o atributo value tenha
     */
    public BasePage verificarValue(String elemento, String valor) {
        $(elemento).shouldBe(value(valor));
        return this;
    }

    /**
     * Verifica se o atributo type possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem a tag type
     * @param valor    o valor que deseja verificar que o atributo type tenha
     */
    public BasePage verificarType(String elemento, String valor) {
        $(elemento).shouldBe(type(valor));
        return this;
    }

    /**
     * Verifica se o atributo id possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem o id
     * @param valor    o valor que deseja verificar que o atributo id tenha
     */
    public BasePage verificarID(String elemento, String valor) {
        $(elemento).shouldBe(id(valor));
        return this;
    }

    /**
     * Verifica se o elemento está vazio
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarElementoVazio(String elemento) {
        $(elemento).shouldBe(empty);
        return this;
    }

    /**
     * Verifica se o atributo class possui um determinado valor
     *
     * @param elemento o elemento que deseja ser verificado que contem a class
     * @param valor    o valor que deseja verificar que o atributo class tenha
     */
    public BasePage verificarClass(String elemento, String valor) {
        $(elemento).shouldBe(cssClass(valor));
        return this;
    }

    /**
     * Verifica se o elemento está selecionado
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemSelecionado(String elemento) {
        $(elemento).shouldBe(selected);
        return this;
    }

    /**
     * Verifica se o elemento possui um determinado valor de texto, sem considerar letras maiusculas ou minusculas
     *
     * @param elemento o elemento que deseja ser verificado que contem o texto
     * @param valor    o valor que deseja verificar que o atributo class tenha
     */
    public BasePage verificarTexto(String elemento, String valor) {
        $(elemento).shouldBe(text(valor));
        return this;
    }

    /**
     * Verifica se o elemento possui um determinado valor de texto, considerando letras maiusculas ou minusculas
     *
     * @param elemento o elemento que deseja ser verificado que contem o texto
     * @param valor    o valor que deseja verificar que o atributo class tenha
     */
    public BasePage verificarTextoCaseSensitive(String elemento, String valor) {
        $(elemento).shouldBe(textCaseSensitive(valor));
        return this;
    }

    /**
     * Verifica se um item está habilitado
     *
     * @param elemento o elemento que deseja ser verificado
     */
    public BasePage verificarItemAtivado(String elemento) {
        $(elemento).shouldBe(enabled);
        return this;
    }

    /**
     * Verifica um texto em uma tabela
     *
     * @param elemento o elemento que deseja ser verificado
     * @param texto    o texto que precisa ser validado na tabela
     */
    public ElementsCollection verificarTextoEmtabela(String elemento, String texto) {
        return $$(By.xpath(elemento)).shouldHave(CollectionCondition.itemWithText(texto));
    }
    /* ---------------------------------->  Fim dos métodos de condições  <------------------------------- */

    /**
     * Arrasta um elemento para um determinado ponto da tela
     *
     * @param elemento o elemento para ser movido
     * @param alvo     onde o elemento tem que ser movido
     */
    public BasePage moverElementosPorXpath(String elemento, String alvo) {
        $(byXpath(elemento)).dragAndDropTo(alvo);
        return this;
    }

    /**
     * Método para incluir um valor em um campo
     *
     * @param elemento O elemento onde deseja incluir um valor
     * @param valor    O valor que voce quer incluir
     */
    public String inserirValor(String elemento, String valor) {
        $(elemento).setValue(valor);
        return null;
    }

    public BasePage inserirValorporXpath(String elemento, String valor) {
        $(byXpath(elemento)).setValue(valor);
        return this;
    }

    public String log(String texto) {
        System.out.println(texto);
        return null;
    }

    public SelenideElement progressoConcluido(String elemento, String valor) {
        return $(by(elemento, valor)).shouldBe(exist);
    }


}

