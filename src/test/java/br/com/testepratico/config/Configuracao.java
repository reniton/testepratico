package br.com.testepratico.config;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "classpath:config.properties"
})
public interface Configuracao extends Config {

    @Config.Key("browser")
    String browser();

    @Config.Key("url")
    String url();

    @Key("timeout")
    int timeout();

    @Key("nome.produto")
    String nome_produto();

    @Key("cor.produto")
    String cor_produto();

    @Key("mensagem.carrinho.vazio")
    String msg_carrinho_vazio();


}

