package br.com.testepratico.attributes;

public interface ProdutosAttributes {
    String SEE_OFFER = "#see_offer_btn";
    String ADD_TO_CART = "button[name='save_to_cart']";
    String SPECIFICATIONS = "//h1[contains(@class,'roboto-regular screen768')]";
    String SEARCH = "#menuSearch";
    String AUTOCOMPLETE = "#autoComplete";
    String CART = "#shoppingCartLink";
    String COLOR = "(//span[@title='GRAY'])[2]";
    String CHOOSE_PRODUCT = "//a[@class='product ng-scope']";
    String QUANTITY = ".plus";
    String UNITARY_VALUE = "//h2[contains(@class,'roboto-thin screen768')]";
    String QUANTITY_VALUES = "//td[@class='smollCell quantityMobile']";
    String TOTAL = "(//span[@class='roboto-medium ng-binding'])[3]";
    String REMOVE = "REMOVE";
    String EMPTY_CART = "//label[@class='roboto-bold ng-scope']";

}
