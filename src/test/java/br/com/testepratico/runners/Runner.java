package br.com.testepratico.runners;

import br.com.testepratico.core.BaseTest;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import org.junit.ClassRule;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(features = "classpath:features",
        glue = {""},
        publish = false,
        // tags = "@VALIDAR_ALTERACAO_COR",
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        monochrome = true,
        dryRun = false,
        plugin = {"json:target/cucumber.json", "rerun:target/rerun.txt"})

public class Runner {
    @ClassRule
    public static BaseTest testRule = new BaseTest();
}
