package br.com.testepratico.page;

import br.com.testepratico.attributes.ProdutosAttributes;
import br.com.testepratico.core.BasePage;


public class ProdutoPage extends BasePage implements ProdutosAttributes {


    public ProdutoPage seeOffer() {
        clicarElemento(SEE_OFFER);
        return this;
    }

    public ProdutoPage addCart() {
        clicarElemento(ADD_TO_CART);
        return this;
    }

    public ProdutoPage selectColor(String cor) {
        clicarElementoXpath("(//span[@title='" + cor + "'])[2]");
        return this;
    }

    public ProdutoPage selectColorDifference(String color) {
        verificarItemVisivel("(//span[@title='" + color + "'])[2]");
        clicarElementoXpath("(//span[@title='" + color + "'])[2]");
        System.out.println(color);
        return this;
    }

    public String validateSpecifications() {
        return recuperarTextoPorXpath(SPECIFICATIONS);
    }

    public Boolean validaColor() {
        hover(CART);
        return verificarItemExistePorXpath(COLOR);
    }

    public ProdutoPage searchProduct(String texto) {
        clicarElemento(SEARCH);
        inserirValor(AUTOCOMPLETE, texto);
        return this;
    }

    public ProdutoPage selectProduct() {
        clicarElementoXpath(CHOOSE_PRODUCT);
        return this;
    }

    public ProdutoPage changeQuantity() {
        clicarElemento(QUANTITY);
        return this;
    }

    public ProdutoPage accessPageCheckout() {
        clicarElemento(CART);
        return this;
    }

    public String getUnitValue() {
        return recuperarTextoPorXpath(UNITARY_VALUE)
                .replace("$", "").replace(".", "");
    }

    public String getSelectedAmount() {
        return recuperarTextoPorXpath(QUANTITY_VALUES);
    }

    public String getTotal() {
        return recuperarTextoPorXpath(TOTAL)
                .replace("$", "").replace(",", "").replace(".", "");
    }

    public ProdutoPage removeProduct() {
        clicarLink(REMOVE);
        return this;
    }

    public String validateEmptyAffection() {
        return recuperarTextoPorXpath(EMPTY_CART);
    }


}
