package br.com.testepratico.steps;

import br.com.testepratico.Utils.Randon;
import br.com.testepratico.core.BaseTest;
import com.codeborne.selenide.Selenide;
import dao.MassaDAO;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import org.junit.Assert;

import static br.com.testepratico.Utils.Randon.randonColor;

public class ProdutosSteps extends BaseTest {

    int valorUnitario;
    String color = randonColor().toString();

    @Dado("que o usuário acesse o site")
    public void queOUsuárioAcesseOSite() {
        start("Chrome");
        Selenide.open("");
    }

    @Quando("clicar na opção Special Offer")
    public void noMenuClicarNaOpçãoSpecialOffer() {
        menu.acessarSpecialOffer();
    }

    @Quando("Alterar a cor do produto de acordo com a cor informada no banco de automação")
    public void alterarACorDoProdutoDeAcordoComACorInformadaNoBancoDeAutomação() {
        produto.selectColor(massa.obterInformacao(dados.cor_produto()));
    }

    @Quando("Alterar a cor do produto para uma diferente da existente no banco de automação")
    public void alterarACorDoProdutoParaUmaDiferenteDaExistenteNoBancoDeAutomação() {
        produto.selectColorDifference(color);
    }

    @Quando("Clicar no botão “Add to cart”")
    public void clicarNoBotãoAddToCart() {
        produto.addCart();
    }

    @Quando("Pesquisar o produto clicando no ícone de lupa \\(Seguir o nome do produto do banco deautomação)")
    public void pesquisarOProdutoClicandoNoÍconeDeLupaSeguirONomeDoProdutoDoBancoDeautomação() {
        produto.searchProduct(massa.obterInformacao(dados.nome_produto()));
    }

    @Quando("Selecionar produto pesquisado")
    public void selecionarProdutoPesquisado() {
        produto.selectProduct();
    }

    @Quando("Alterar a quantidade de produtos que deseja comprar")
    public void alterarAQuantidadeDeProdutosQueDesejaComprar() {
        valorUnitario = Integer.parseInt(produto.getUnitValue());
        produto.changeQuantity();
    }

    @Quando("Acessar a página de checkout")
    public void acessarAPáginaDeCheckout() {
        produto.accessPageCheckout();
    }

    @Quando("Clicar no carrinho de compras")
    public void clicarNoCarrinhoDeCompras() {
        produto.accessPageCheckout();
    }

    @Quando("Remover produto do carrinho de compras")
    public void removerProdutoDoCarrinhoDeCompras() {
        produto.removeProduct();
    }


    @E("Clicar no botão See offer")
    public void ClicarNoBotãoSeeOffer() {
        produto.seeOffer();
    }

    @Então("Validar que as especificações do produto de acordo com as informações retornadas do banco de automação")
    public void validarQueAsEspecificaçõesDoProdutoDeAcordoComAsInformaçõesRetornadasDoBancoDeAutomação() {
        Assert.assertEquals(massa.obterInformacao(dados.nome_produto()), produto.validateSpecifications());
        finish();
    }


    @Então("Validar que o carrinho de compras está vazio")
    public void validarQueOCarrinhoDeComprasEstáVazio() {
        Assert.assertEquals(dados.msg_carrinho_vazio(), produto.validateEmptyAffection());
        finish();
    }

    @Então("Validar que produto foi adicionado ao carrinho com a cor selecionada")
    public void validarQueProdutoFoiAdicionadoAoCarrinhoComACorSelecionada() {
        Assert.assertTrue(produto.validaColor());
        finish();

    }

    @Então("Validar que a soma dos preços corresponde ao total apresentado na página de checkout")
    public void validarQueASomaDosPreçosCorrespondeAoTotalApresentadoNaPáginaDeCheckout() {
        System.out.println("Valor Unitário: " + valorUnitario);
        System.out.println("Quantidade: " + produto.getSelectedAmount());
        System.out.println("Total: " + produto.getTotal());
        int valorQuantidade = Integer.parseInt(produto.getSelectedAmount());
        int total = Integer.parseInt(produto.getTotal());
        int valorTotal = valorUnitario * valorQuantidade;
        Assert.assertEquals(valorTotal, total);
        finish();
    }

    @Então("Realizar um update no banco de automação alterar a cor existente no banco para a cor selecionada no teste.")
    public void realizarUmUpdateNoBancoDeAutomaçãoAlterarACorExistenteNoBancoParaACorSelecionadaNoTeste() {
        System.out.println("Vai salvar no banco: " + color);
        massa.alterarInformacao(color);
    }
}
