#language: pt

Funcionalidade: Validar itens do produto

  @VALIDAR_ESPECIFICACOES
  Cenário: Validar especificações do produto
    Dado que o usuário acesse o site
    Quando clicar na opção Special Offer
    E Clicar no botão See offer
    Então Validar que as especificações do produto de acordo com as informações retornadas do banco de automação


  @VALIDAR_ALTERACAO_COR
  Cenário: Validar alteração de cor do produto no carrinho

    Dado que o usuário acesse o site
    Quando clicar na opção Special Offer
    E Clicar no botão See offer
    E Alterar a cor do produto de acordo com a cor informada no banco de automação
    E Clicar no botão “Add to cart”
    Então Validar que produto foi adicionado ao carrinho com a cor selecionada

  @VALIDAR_PAGINA_CHECKOUT
  Cenário:  Validar página de checkout

    Dado que o usuário acesse o site
    Quando  Pesquisar o produto clicando no ícone de lupa (Seguir o nome do produto do banco deautomação)
    E Selecionar produto pesquisado
    E Alterar a cor do produto para uma diferente da existente no banco de automação
    E Alterar a quantidade de produtos que deseja comprar
    Quando Clicar no botão “Add to cart”
    E Acessar a página de checkout
    Então Validar que a soma dos preços corresponde ao total apresentado na página de checkout
    E Realizar um update no banco de automação alterar a cor existente no banco para a cor selecionada no teste.


  @VALIDAR_REMOCAO_CARRINHO
  Cenário: Remover produto do carrinho de compras

    Dado que o usuário acesse o site
    E clicar na opção Special Offer
    E Clicar no botão See offer
    E Clicar no botão “Add to cart”
    Quando Clicar no carrinho de compras
    E Remover produto do carrinho de compras
    Então Validar que o carrinho de compras está vazio

